//
//  Timer.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/3/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import UIKit

@objc protocol TimerDelegate {
    @objc optional func tick()
    
    @objc optional func timerStarted()
    
    @objc optional func timerStopped()
}

class Timer: NSObject {
    
    // MARK: Private properties
    
    fileprivate var startTime = Date()
    
    fileprivate var timer = Foundation.Timer()
    
    fileprivate var delegates = [TimerDelegate]()
    
    // MARK: Private methods
    @objc fileprivate func parentTick() {
        
        
        // Call delegate tick method
        for del in delegates {
            del.tick?()
        }
    }
    
    // MARK: Public properties
    
    var elapsedTime: Time {
        get{
            if running {
                return Time(Date().timeIntervalSince(startTime))
            }
            else {
                return Time(0)
            }
        }
    }
    
    var tickDuration = TimeInterval(0.01) {
        didSet{
            if tickDuration < 0.000 {
                tickDuration = oldValue
            }
        }
    }
    
    
    
    var running: Bool {
        get{
            return timer.isValid
        }
    }
    
    // MARK: Public methods
    
    func addDelegate(delegate: TimerDelegate) {
        delegates.append(delegate)
    }
    
    func removeDelegate(delegate: TimerDelegate) {
        delegates = delegates.filter() { $0 !== delegate }
    }
    
    func start() {
        if !running {
            // Start the timer
            timer = Foundation.Timer.scheduledTimer(timeInterval: tickDuration, target: self, selector: #selector(Timer.parentTick), userInfo: nil, repeats: true)
            for del in delegates {
                del.timerStarted?()
            }
            startTime = Date()
        }
    }
    
    func stop() {
        if running {
            timer.invalidate()
            for del in delegates {
                del.timerStopped?()
            }
        }
    }
    
    func restart() {
        if running {
            timer.invalidate()
            timer = Foundation.Timer.scheduledTimer(timeInterval: tickDuration, target: self, selector: #selector(Timer.parentTick), userInfo: nil, repeats: true)
            startTime = Date()
        }
    }
}

