//
//  SettingsTableViewController.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/10/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, UITextFieldDelegate {

    // MARK: Properties
    var settings: Settings = Settings.readSettings()
    
    // MARK: Outlets
    @IBOutlet weak var timeIntervalCell: UITableViewCell!
    @IBOutlet weak var timeIntervalLabel: UILabel!
    @IBOutlet weak var volumeMultiplierField: UITextField!
    @IBOutlet weak var unitSuffixField: UITextField!
    @IBOutlet weak var maximumAverageField: UITextField!
    
    // MARK: Actions
    @IBAction func resetToDefaults(_ sender: UIButton) {
        let alert = UIAlertController(title: "Are you sure?", message: "This will clear all your settings and insert default values", preferredStyle: .alert)
        
        let preferedAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(preferedAction)
        alert.preferredAction = preferedAction
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) -> Void in
            self.settings.reset()
            self.updateFields()
            })
        
        present(alert, animated: true, completion: {self.updateFields()})
        
    }
    
    func finishEditing() {
        view.endEditing(true)
    }
    
    // MARK: TableView
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) == timeIntervalCell {
            let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            
            alert.addAction(
                UIAlertAction(title: "Seconds", style: .default, handler: {(UIAlertAction) -> Void in
                    self.settings.timeInterval = .Seconds
                    self.updateFields()
                }))
            alert.addAction(
                UIAlertAction(title: "Minutes", style: .default, handler: {(UIAlertAction) -> Void in
                    self.settings.timeInterval = .Minutes
                    self.updateFields()
                }))
            alert.addAction(
                UIAlertAction(title: "Hours", style: .default, handler: {(UIAlertAction) -> Void in
                    self.settings.timeInterval = .Hours
                    self.updateFields()
                }))
            alert.addAction(
                UIAlertAction(title: "Days", style: .default, handler: {(UIAlertAction) -> Void in
                    self.settings.timeInterval = .Days
                    self.updateFields()
                }))
            
            alert.popoverPresentationController?.sourceView = timeIntervalLabel
            
            present(alert, animated: true)
        }
    }
    
    // MARK: Custom Methods
    func updateFields() {
        
        switch settings.timeInterval {
        case .Days:
            timeIntervalLabel.text = "Days"
        case .Hours:
            timeIntervalLabel.text = "Hours"
        case .Minutes:
            timeIntervalLabel.text = "Minutes"
        case .Seconds:
            timeIntervalLabel.text = "Seconds"
        }
        
        volumeMultiplierField.text = String(format: "%f", settings.multiplier)
        unitSuffixField.text = settings.suffix
        maximumAverageField.text = String(format: "%d", settings.historyMax)
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case volumeMultiplierField:
            if let newMultiplier = Double(volumeMultiplierField.text!) {
                settings.multiplier = newMultiplier
            } else {
                updateFields()
            }
        case unitSuffixField:
            settings.suffix = unitSuffixField.text ?? "GPM"
        case maximumAverageField:
            if let historyMax = Int(maximumAverageField.text!) {
                settings.historyMax = historyMax
            } else {
                updateFields()
            }
        default:
            updateFields()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: UITableViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
        
        numberToolbar.items = [UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(finishEditing))]
        numberToolbar.semanticContentAttribute = .forceRightToLeft
        
        // Set up text field delegates
        volumeMultiplierField.delegate = self
        volumeMultiplierField.inputAccessoryView = numberToolbar
        unitSuffixField.delegate = self
        maximumAverageField.delegate = self
        maximumAverageField.inputAccessoryView = numberToolbar
        
        // update fields
        updateFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        settings = Settings.readSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // close any keyboards to update settings
        finishEditing()
        
        // Save the settings
        Settings.saveSettings(settings: settings)
    }

}
