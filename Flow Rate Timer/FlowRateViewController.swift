//
//  FlowRateViewController.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/3/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import UIKit
import Firebase

class FlowRateViewController: UIViewController, TimerDelegate{

    // MARK: Outlets
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var stopButton: UIButton!
    
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var gpmLabel: UILabel!
    
    @IBOutlet weak var adBanner: GADBannerView!
    
    // MARK: Properties
    
    let playImage = UIImage(named: "play")
    
    let repeatImage = UIImage(named: "repeat")
    
    let rateUnit = "GPM"
    
    let rateMultiplier = 100.0
    
    var timer = SimpleFlowTimer()
//    var startTime = NSTimeInterval()
//    
//    var elapsedTime = NSTimeInterval()
//    
//    var timer = NSTimer()
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        stopButton.isEnabled = false
        timer.addDelegate(delegate: self)
        
        // set up ads
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID,
                                "6fd43c6e4d75ae36b3a50c9d6aa2031e"] // My iPhone
        adBanner.adUnitID = "ca-app-pub-4168470011470638/4565669701"
        adBanner.rootViewController = self
        adBanner.load(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("received memory warning!!")
    }
    
    // MARK: Button Actions
    
    @IBAction func stopTimer(_ sender: UIButton) {
//        timer.invalidate()
        timer.stop()
        timerLabel.text = "00:00.00"
        startButton.setImage(playImage, for: UIControlState())
        stopButton.isEnabled = false
        stopButton.backgroundColor = nil
        settingsButton.isEnabled = true
    }

    @IBAction func resetTimer(_ sender: UIButton) {
        stopTimer(sender)
        gpmLabel.text = timer.zeroString
    }
    
    @IBAction func startAndUpdateTimer(_ sender: UIButton) {
        if !timer.running {
            // Start the timer
//            timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: #selector(FlowRateViewController.updateTime), userInfo: nil, repeats: true)
//            startTime = NSDate.timeIntervalSinceReferenceDate()
            timer.start()
            settingsButton.isEnabled = false
            startButton.setImage(repeatImage, for: UIControlState())
            stopButton.isEnabled = true
            stopButton.backgroundColor = UIColor.red
        }
        else {
            // Update the GPM display
            
            gpmLabel.text = timer.calculate()
        }
    }
    // MARK: TimerDelegate
    func tick() {
        timerLabel.text = timer.elapsedTime.toString
    }
    
    // MARK: Custom methods
//    func updateTime() {
//        let currentTime = NSDate.timeIntervalSinceReferenceDate()
//        
//        // Find the difference between current time and start time.
//        elapsedTime = currentTime - startTime
//        var elapsedtime = elapsedTime
//        
//        // calculate the minutes in elapsed time.
//        let minutes = UInt8(elapsedtime / 60.0)
//        
//        elapsedtime -= (NSTimeInterval(minutes) * 60)
//        
//        // calculate the seconds in elapsed time.
//        let seconds = UInt8(elapsedtime)
//        
//        elapsedtime -= NSTimeInterval(seconds)
//        
//        // find out the fraction of milliseconds to be displayed.
//        let fraction = UInt8(elapsedtime * 100)
//        
//        // add the leading zero for minutes, seconds, and milliseconds
//        let minuteStr = String(format: "%02d", minutes)
//        let secondStr = String(format: "%02d", seconds)
//        let fractionStr = String(format: "%02d", fraction)
//        
//        // concatenate minutes, seconds and milliseconds
//        timerLabel.text = "\(minuteStr):\(secondStr).\(fractionStr)"
//    }
}
