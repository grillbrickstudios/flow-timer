//
//  SimpleFlowTimer.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/8/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import UIKit

class SimpleFlowTimer: Timer, TimerDelegate{
    // MARK: Properties
    
    fileprivate var multiplier: Double {
        get{
            return Settings.readSettings().multiplier
        }
    }
    
    fileprivate var suffix: String {
        get{
            return Settings.readSettings().suffix
        }
    }
    
    fileprivate var maxHistory: Int {
        get{
            return Settings.readSettings().historyMax
        }
    }
    
    fileprivate var timeInterval: TimeUnit {
        get{
            return Settings.readSettings().timeInterval
        }
    }
    
    fileprivate func denominator(_ time: Time) -> Double {
        switch timeInterval {
        case .Days:
            return time.totalDays
        case .Hours:
            return time.totalHours
        case .Minutes:
            return time.totalMinutes
        case .Seconds:
            return time.totalSeconds
        }
    }
    var history = [Time]()
    
    // MARK: Initializer
    override init(){
        super.init()
        super.addDelegate(delegate: self)
    }
    // MARK: Public Interface

    var zeroString: String{
        get{
            return String(format: "%02d", 0) + " " + suffix
        }
    }
    
    func calculate() -> String {
        // add current elapsedTime to history
        history.append(elapsedTime)
        
        // if exceeding max history remove the first
        if history.count > maxHistory {
            history.remove(at: 0)
        }
        
        let rate: Int
        
        
        // if maxHistor.count = 1
        if history.count == 1 {
            rate = Int(multiplier / denominator(history[0]))
            restart()
            return String(format: "%02d", rate) + " " + suffix
        } else {
            var total = 0
            for time in history {
                total += Int(multiplier / denominator(time))
            }
            rate = total / history.count
            restart()
            return String(format: "%d %@ (Avg. of %d)", rate, suffix, history.count)
        }
        
    }
    
    func resetToDefault() {
        Settings.readSettings().reset()
    }
    
    // MARK: TimerDelegate
    
    func timerStopped() {
        history.removeAll()
    }
}
