//
//  Settings.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/10/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import UIKit

enum TimeUnit: Int{
    case Seconds, Minutes, Hours, Days
}

class Settings : NSObject, NSCoding{
    // MARK: Private constants
    fileprivate static let multiplierKey = "multiplier"
    fileprivate static let suffixKey = "suffix"
    fileprivate static let historyMaxKey = "historyMax"
    fileprivate static let keyTimeInterval = "timeInterval"
    
    fileprivate static let defaultMultiplier: Double = 100
    fileprivate static let defaultSuffix = "GPM"
    fileprivate static let defaultHistoryMax: Int = 10
    fileprivate static let defaultTimeInterval: TimeUnit = .Minutes
    
    // MARK: Setting variables
    var multiplier: Double
    var suffix: String
    var historyMax: Int
    var timeInterval: TimeUnit
    
    // MARK: Initializers
    init(_ suffix: String, multiplier: Double, historyMax: Int, timeInterval: TimeUnit) {
        self.suffix = suffix
        self.multiplier = multiplier
        self.historyMax = historyMax
        self.timeInterval = timeInterval
        super.init()
    }
    
    convenience override init(){
        self.init(Settings.defaultSuffix, multiplier: Settings.defaultMultiplier, historyMax: Settings.defaultHistoryMax, timeInterval: Settings.defaultTimeInterval)
    }
    
    // MARK: Object Methods
    func reset() {
        multiplier = Settings.defaultMultiplier
        suffix = Settings.defaultSuffix
        historyMax = Settings.defaultHistoryMax
        timeInterval = Settings.defaultTimeInterval
    }
    
    // MARK: Class Methods
    fileprivate class func getPath() -> String {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("FlowTimer.settings").path
    }
    
    class func saveSettings(settings: Settings) {
        NSKeyedArchiver.archiveRootObject(settings, toFile: Settings.getPath())
    }
    
    class func readSettings() -> Settings {
        return NSKeyedUnarchiver.unarchiveObject(withFile: getPath()) as? Settings ?? Settings()
    }
    
    // MARK: NSCoding
    required init?(coder aDecoder: NSCoder) {
        self.multiplier = aDecoder.decodeDouble(forKey: Settings.multiplierKey)
        if let suffix = aDecoder.decodeObject(forKey: Settings.suffixKey) as? String {
            self.suffix = suffix
        } else {
            self.suffix = Settings.defaultSuffix
        }
        self.historyMax = aDecoder.decodeInteger(forKey: Settings.historyMaxKey)
        self.timeInterval = TimeUnit(rawValue: aDecoder.decodeInteger(forKey: Settings.keyTimeInterval)) ?? .Minutes
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(multiplier, forKey: Settings.multiplierKey)
        aCoder.encode(suffix, forKey: Settings.suffixKey)
        aCoder.encode(historyMax, forKey: Settings.historyMaxKey)
        aCoder.encode(timeInterval.rawValue, forKey: Settings.keyTimeInterval)
    }
}
