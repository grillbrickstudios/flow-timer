//
//  Extensions.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/8/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import Foundation

extension Double {
    var toInt: Int {
        get{
            return Int(self)
        }
    }
}