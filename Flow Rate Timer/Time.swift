//
//  Time.swift
//  Flow Rate Timer
//
//  Created by Jim Hessin on 9/8/16.
//  Copyright © 2016 GrillbrickStudios. All rights reserved.
//

import Foundation

struct Time {
    let totalSeconds: Double
    
    init(_ value: Double){
        totalSeconds = value
    }
    
    init(_ value: Int){
        totalSeconds = Double(value)
    }
    
    var seconds: Double {
        get{
            return totalSeconds.truncatingRemainder(dividingBy: 60)
        }
    }
    
    
    var totalMinutes: Double {
        get{
            return totalSeconds / 60
        }
    }
    
    var minutes: Double {
        get{
            let min = totalMinutes.truncatingRemainder(dividingBy: 60)
            return min
        }
    }
    
    var totalHours: Double {
        get{
            return totalMinutes / 60
        }
    }
    
    var hours: Double {
        get{
            return totalHours.truncatingRemainder(dividingBy: 24)
        }
    }
    
    var totalDays: Double {
        get{
            return totalHours / 24
        }
    }
    
    var days: Double {
        get{
            return totalDays.truncatingRemainder(dividingBy: 7)
        }
    }
    
    var totalWeeks: Double {
        return totalDays / 7
    }
    
    var milliseconds: Double {
        get {
            return (totalSeconds - trunc(totalSeconds)) * 100
        }
    }
    
    var toString: String {
        get {
            let hrsInt = hours.toInt
            
            if hrsInt > 0 {
                return String(format: "%02d:%02d:%02d", totalHours.toInt, minutes.toInt, seconds.toInt)
            } else {
                return String(format: "%02d:%02d.%02d", minutes.toInt, seconds.toInt, milliseconds.toInt)
            }
        }
    }
}
